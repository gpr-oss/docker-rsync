FROM alpine:3.5

# We'll likely need to add SSL root certificates
RUN apk --no-cache add rsync openssh-client

ENTRYPOINT ["/usr/bin/rsync"]
